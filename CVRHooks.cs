using System;
using System.Reflection;
using ABI_RC.Core.Player;
using ABI_RC.Core.Savior;
using HarmonyLib;
using UnityEngine;

namespace VibeGoesBrrr
{
  static class CVRHooks
  {
    public class AvatarEventArgs : EventArgs
    {
      public GameObject Avatar;
      public PlayerDescriptor Player;
    }

    public static event EventHandler<AvatarEventArgs> AvatarIsReady;

    public static GameObject LocalAvatar = null;

    public static void OnApplicationStart(HarmonyLib.Harmony harmony)
    {
      harmony.Patch(typeof(PlayerSetup).GetMethod(nameof(PlayerSetup.SetupAvatar), BindingFlags.Public | BindingFlags.Instance), 
        postfix: new HarmonyMethod(typeof(CVRHooks).GetMethod(nameof(OnLocalAvatarLoad), BindingFlags.NonPublic | BindingFlags.Static)));
      harmony.Patch(typeof(PuppetMaster).GetMethod(nameof(PuppetMaster.AvatarInstantiated), BindingFlags.Public | BindingFlags.Instance), 
        postfix: new HarmonyMethod(typeof(CVRHooks).GetMethod(nameof(OnRemoteAvatarLoad), BindingFlags.NonPublic | BindingFlags.Static)));
    }

    private static void OnLocalAvatarLoad(GameObject __0)
    {
      Util.DebugLog("OnLocalAvatarLoad");
      LocalAvatar = __0;
      AvatarIsReady?.Invoke(null, new AvatarEventArgs { Avatar = __0, Player = null });
    }

    private static void OnRemoteAvatarLoad(PuppetMaster __instance)
    {
      var playerDescriptor = Traverse.Create(__instance).Field("_playerDescriptor").GetValue<PlayerDescriptor>();
      Util.DebugLog($"OnRemoteAvatarLoad: {playerDescriptor.userName}");
      AvatarIsReady?.Invoke(null, new AvatarEventArgs { Avatar = __instance.avatarObject, Player = playerDescriptor });
    }

    public static void Vibrate(float delay = 0.0f, float duration = 0.0f, float frequency = 440f, float amplitude = 1f, bool hand = false)
    {
      CVRInputManager.Instance.Vibrate(delay, duration, frequency, amplitude, hand);
    }
  }
}